
module.exports = class Util {

    static wrapResponse(data, status = 200, msg ) {

        if (data === 'err'){
            status = 500;
            msg = msg ? msg : 'Ocorreu um erro na aplicação';
            data = null;
        }

        if(!msg) {
            switch (status) {
                case 412 :
                    msg = 'Dados insuficientes';
                    break;
                case 404 :
                    msg = 'Documento não encontrado';
                    break;
                case 200 :
                    msg = 'Solicitação realizada com sucesso';
                    break;
            }
        }
        return {status: status, msg: msg, data: data};
    }

    
    static modificadorAttr(valor) {

        switch (true) {
            case valor >= 9 && valor <= 10:  return -1;
            case valor >= 11 && valor <= 12: return 0;
            case valor >= 13 && valor <= 15: return 1;
            case valor >= 16 && valor <= 18: return 2;
            case valor >= 19: return 3;
            default: return -2;
        }
        
    }

}