const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const WeaponSchema = new Schema({
    name: {type: String, required: true, unique: true},
    mod: {type: Number,  required: true},
    attr: {type: String, required: true},
    equipped: {type: Boolean, default: false}
});

const KnightSchema = new Schema({
        name: { type: String, required: true, unique: true},
        nickName: { type: String, required: true},
        birthday: { type: Number, default:0},
        exp: {type : Number, default:0},
        alive: {type : Boolean, default:true, select: false},

        weapons: [WeaponSchema],

        keyAttribute: { type: String, default: 'strength'},      

        attributes: { type: Map , of: Number, default: new Map([
            ['strength',0],
            ['dexterity', 0],
            ['constitution', 0],
            ['intelligence', 0],
            ['wisdom', 0],
            ['charisma', 0]
        ])}
    });

module.exports = mongoose.model('Knight', KnightSchema);

