const util = require('../util/util');

class KnightOut {
    constructor(knight) {
        this.idade = knight.birthday;
        this.nome = knight.name;
        this.atributo = knight.keyAttribute;
        let valorAtributo = knight.attributes.get(this.atributo);
        let equippedWeapon = knight.weapons.find(obj=>obj.equipped);
        this.ataque = 10 + util.modificadorAttr(valorAtributo) + (equippedWeapon ? equippedWeapon.mod : 0);        
        this.exp = this.idade < 7 ?  0 : Math.floor((this.idade - 7) * Math.pow(22, 1.45));
        this.armas = knight.weapons.length;
    }
}

module.exports = KnightOut;