const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const config = require('./config/config');
const options = { reconnectTries: Number.MAX_VALUE, reconnectInterval: 500, poolSize: 5, useNewUrlParser: true, useUnifiedTopology: true };

const url = config.database;

mongoose.connect(url, options);
mongoose.set('useCreateIndex', true);

mongoose.connection.on('error', ()=> {
    console.log('Falha na conexão com o MongoDB');
});

mongoose.connection.on('disconnected', ()=> {
    console.log('Aplicação desconectada do MongoDB');
});

mongoose.connection.on('connected', ()=> {
    console.log('Aplicação conectada ao MongoDB');
});

app.use(bodyParser.urlencoded( { extended: false}));
app.use(bodyParser.json());

const knightRoute = require('./routes/knights');

app.use('/knights', knightRoute);
app.listen(3000);

module.exports = app;