const express = require('express');
const router = express.Router();
const knights = require('../model/knight');
const util = require('../util/util');
const { check, validationResult } = require('express-validator');
const KnightOut = require('../model/knightOut');

router.get('/', (req, res) => {
    let filter = escape(req.query.filter) == escape('heroes') ? false : true;
    knights.find({alive:filter}, (err, data) => {
        if(err) {
            return res.status(500).send(util.wrapResponse('err'));
        }
        return res.status(200).send(util.wrapResponse(constructObjOut(data), 200));
    });
});

router.get('/:id', (req, res) => {
    var id = req.params.id;
    knights.findById(id, (err, data) => {
        if(err) {
            return res.status(404).send(util.wrapResponse(null, 404));
        }
        return res.status(200).send(util.wrapResponse(constructObjOut(data), 200));
    }).where({alive:true});
});

let validaKnight = [
    check('name').exists(),
    check('nickName').exists()
];

router.post('/', validaKnight, async (req, res) => {
    const errors = validationResult(req);
    const { name, nickName } = req.body;
    
    if (!errors.isEmpty()) {
        return res.status(412).send(util.wrapResponse(null, 412));
    }

    try {
        if (await knights.findOne({name})) {
            return res.status(412).send(util.wrapResponse(null, 412, 'Já existe um guerreiro com este nome'));
        }

        let knight = await knights.create({name, nickName});

        return res.status(201).send(util.wrapResponse(knight, 201));

    } catch (err) {
        console.log(err);
        return res.status(500).send(util.wrapResponse('err', 500, 'erro na criação do guerreiro'));
    }
});

router.put('/:id', check('nickName').exists(), async (req, res) => {
    const errors = validationResult(req);
    const { nickName } = req.body;
    const id = req.params.id;
    
    if (!errors.isEmpty()) {
        return res.status(412).send(util.wrapResponse(null, 412));
    }
    
    try {
        let knight = await knights.findByIdAndUpdate(id, {nickName: escape(nickName)}, {new: true});
        return res.status(200).send(util.wrapResponse(knight, 200));

    } catch (err) {
        if (err.name === 'DocumentNotFoundError'){
            return res.status(404).send(util.wrapResponse(null, 404));
        }
        return res.status(500).send(util.wrapResponse('err'));
    }
});

router.delete('/:id', async (req, res) => {
    const id = req.params.id;    
    try {
        let knight = await knights.findByIdAndUpdate(id, {alive: false}, {new: true});
        return res.status(200).send(util.wrapResponse(knight, 200));

    } catch (err) {
        if (err.name === 'DocumentNotFoundError'){
            return res.status(404).send(util.wrapResponse(null, 404));
        }
        return res.status(500).send(util.wrapResponse('err'));
    }
});

function constructObjOut(data) {
    obj = {};
    
    try {
        if (Array.isArray(data)) {
            return data.map((knight) => {
                return new KnightOut(knight);
            });
        } else if(data){
            return new KnightOut(data);
        }
    } catch (err) {
        console.log("LOG Erro - erro na construção do objeto de saida knight", err);
        return obj;
    }
}

module.exports = router;
